import subprocess


subprocess.run('mkdir /home/danimir/.local/bin', shell=True)
with open ('/home/danimir/.bashrc', 'r') as bash_conf:
    new_bash_conf = bash_conf.read() + 'export PATH=$PATH:/usr/sbin\nexport PATH=$PATH:/home/danimir/.local/bin'
with open ('/home/danimir/.bashrc', 'w') as bash_conf:
    bash_conf.write(new_bash_conf)
    