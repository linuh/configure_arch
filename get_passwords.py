import subprocess


# def get_users():
#     users = subprocess.run(['ls', '/home'], capture_output=True)
#     return users.stdout.decode('utf-8').split()


# def get_users_passwords(users: list[str]):
#     return {
#         user: input(f'Enter {user} password: ')
#         for user in users
#     }


def get_user():
    user = subprocess.run(['ls', '/home'], capture_output=True)
    return user.stdout.decode('utf-8')


def get_user_password(user: str):
    return input(f'Enter {user[0:-1]} password: ')


def get_root_password():
    return input('Enter root password: ')

