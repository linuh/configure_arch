import subprocess


flatapps = ['flatseal', 'com.github.tchx84.Flatseal',
            'PyCharm', 'com.jetbrains.PyCharm-Community',
            'vieb', 'dev.vieb.Vieb',
            'librewolf', 'io.gitlab.librewolf-community',
            'obsidian', 'md.obsidian.Obsidian',
            'libreoffice', 'org.libreoffice.LibreOffice',
            'qbittorrent', 'org.qbittorrent.qBittorrent',
            ]
aliases = ''
for i in range(len(flatapps)//2):
    aliases += f'alias {flatapps[i*2]}="flatpak run {flatapps[i*2+1]}"\n'
with open('/home/danimir/.bashrc', 'r') as bash_conf:
    new_bash_conf = bash_conf.read() + f'export PATH=$PATH:/usr/sbin\n{aliases}'
with open('/home/danimir/.bashrc', 'w') as bash_conf:
    bash_conf.write(new_bash_conf)

subprocess.run(f'echo "werk432" | su -c "usermod -aG sudo danimir"', shell=True)

with open('/etc/sudoers', 'r') as file:
    sudoers = file.read()
with open('/etc/sudoers', 'w') as file:
    file.write(sudoers + 'danimir ALL=(ALL) NOPASSWD:ALL')

subprocess.run('sudo apt-get install python3-poetry htop neovim flatpak neofetch qemu-system git tmux ranger -y',
               shell=True)
subprocess.run('flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo',
               shell=True)
subprocess.run(f'flatpak install {" ".join(list(app for app in flatapps[1::2]))} -y',
               shell=True)
subprocess.run('sudo apt-get purge firefox-esr libreoffice-base-core libreoffice-draw libreoffice-kf5 libreoffice-style-breeze libreoffice-calc libreoffice-help-common libreoffice-math libreoffice-style-colibre libreoffice-common libreoffice-help-en-us libreoffice-plasma libreoffice-writer libreoffice-core libreoffice-impress libreoffice-qt5', shell=True)

with open('/etc/sudoers', 'w') as file:
    file.write(sudoers)

subprocess.run('reboot')
