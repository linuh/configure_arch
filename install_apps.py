import subprocess

subprocess.run('sudo apt-get install python3-poetry htop neovim flatpak neofetch qemu-system git tmux ranger -y',
               shell=True)
subprocess.run('flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo',
               shell=True)
subprocess.run('flatpak install flathub com.github.tchx84.Flatseal com.jetbrains.PyCharm-Community dev.vieb.Vieb io.gitlab.librewolf-community md.obsidian.Obsidian org.libreoffice.LibreOffice org.qbittorrent.qBittorrent',
               shell=True)
